// Generated code from Butter Knife. Do not modify!
package com.messenger.inwady.myssenger.fragment.content;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class Contacts$$ViewBinder<T extends com.messenger.inwady.myssenger.fragment.content.Contacts> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131493028, "field 'listView'");
    target.listView = finder.castView(view, 2131493028, "field 'listView'");
    view = finder.findRequiredView(source, 2131492978, "field 'editEmail'");
    target.editEmail = finder.castView(view, 2131492978, "field 'editEmail'");
    view = finder.findRequiredView(source, 2131492979, "field 'editPhone'");
    target.editPhone = finder.castView(view, 2131492979, "field 'editPhone'");
    view = finder.findRequiredView(source, 2131493026, "field 'buttonSearch'");
    target.buttonSearch = finder.castView(view, 2131493026, "field 'buttonSearch'");
    view = finder.findRequiredView(source, 2131493031, "field 'buttonExport'");
    target.buttonExport = finder.castView(view, 2131493031, "field 'buttonExport'");
    view = finder.findRequiredView(source, 2131493029, "field 'loading'");
    target.loading = finder.castView(view, 2131493029, "field 'loading'");
    view = finder.findRequiredView(source, 2131493030, "field 'nothingContacts'");
    target.nothingContacts = finder.castView(view, 2131493030, "field 'nothingContacts'");
  }

  @Override public void unbind(T target) {
    target.listView = null;
    target.editEmail = null;
    target.editPhone = null;
    target.buttonSearch = null;
    target.buttonExport = null;
    target.loading = null;
    target.nothingContacts = null;
  }
}
