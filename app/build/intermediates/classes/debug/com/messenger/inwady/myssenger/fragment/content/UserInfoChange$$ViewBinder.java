// Generated code from Butter Knife. Do not modify!
package com.messenger.inwady.myssenger.fragment.content;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class UserInfoChange$$ViewBinder<T extends com.messenger.inwady.myssenger.fragment.content.UserInfoChange> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131493048, "field 'nickView'");
    target.nickView = finder.castView(view, 2131493048, "field 'nickView'");
    view = finder.findRequiredView(source, 2131493049, "field 'editStatus'");
    target.editStatus = finder.castView(view, 2131493049, "field 'editStatus'");
    view = finder.findRequiredView(source, 2131493050, "field 'editEmail'");
    target.editEmail = finder.castView(view, 2131493050, "field 'editEmail'");
    view = finder.findRequiredView(source, 2131493051, "field 'editPhone'");
    target.editPhone = finder.castView(view, 2131493051, "field 'editPhone'");
    view = finder.findRequiredView(source, 2131493052, "field 'buttonAttach'");
    target.buttonAttach = finder.castView(view, 2131493052, "field 'buttonAttach'");
    view = finder.findRequiredView(source, 2131493053, "field 'save'");
    target.save = finder.castView(view, 2131493053, "field 'save'");
    view = finder.findRequiredView(source, 2131493047, "field 'imageView'");
    target.imageView = finder.castView(view, 2131493047, "field 'imageView'");
  }

  @Override public void unbind(T target) {
    target.nickView = null;
    target.editStatus = null;
    target.editEmail = null;
    target.editPhone = null;
    target.buttonAttach = null;
    target.save = null;
    target.imageView = null;
  }
}
