// Generated code from Butter Knife. Do not modify!
package com.messenger.inwady.myssenger.fragment.content;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class UserInfo$$ViewBinder<T extends com.messenger.inwady.myssenger.fragment.content.UserInfo> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131493044, "field 'status'");
    target.status = finder.castView(view, 2131493044, "field 'status'");
    view = finder.findRequiredView(source, 2131493043, "field 'nick'");
    target.nick = finder.castView(view, 2131493043, "field 'nick'");
    view = finder.findRequiredView(source, 2131493045, "field 'email'");
    target.email = finder.castView(view, 2131493045, "field 'email'");
    view = finder.findRequiredView(source, 2131493046, "field 'phone'");
    target.phone = finder.castView(view, 2131493046, "field 'phone'");
  }

  @Override public void unbind(T target) {
    target.status = null;
    target.nick = null;
    target.email = null;
    target.phone = null;
  }
}
