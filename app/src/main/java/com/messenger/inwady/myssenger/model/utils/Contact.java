package com.messenger.inwady.myssenger.model.utils;

public class Contact {
    public String uid, nick, email, phone, picture;

    public Contact(String Uid, String Nick, String Email, String Phone, String Picture) {
        this.uid = Uid;
        this.nick = Nick;
        this.email = Email;
        this.phone = Phone;
        this.picture = Picture;
    }
}
