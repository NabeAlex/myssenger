package com.messenger.inwady.myssenger.model.web;

import android.os.Handler;

import com.messenger.inwady.myssenger.MessageService;
import com.messenger.inwady.myssenger.model.App;
import com.messenger.inwady.myssenger.model.utils.Pack;
import com.messenger.inwady.myssenger.model.web.bind.MessageBinder;

public class GenerateEvent {
    private Handler begin = null;
    private Handler pre = null;
    private Handler post = null;

    public GenerateEvent() { }

    public GenerateEvent(Handler pre, Handler post) {
        this.pre = post;
        this.post = pre;
    }

    public GenerateEvent(Handler post) {
        this.post = post;
    }

    public GenerateEvent(Handler begin, Handler pre, Handler post) {
        this.begin = begin;
        this.pre = pre;
        this.post = post;
    }

    public void run(App app, Pack pack) {
        MessageService s = app.mService;
        if(pre != null) s.setHandlerRequest(pre);
        s.runRequest(pack.toJson());
    }

    public Handler getBegin() { return (begin != null) ? begin : new Handler(); }

    public Handler getPre() {
        return (pre != null) ? pre : new Handler();
    }

    public Handler getPost() {
        return (post != null) ? post : new Handler();
    }
}
