package com.messenger.inwady.myssenger.fragment.content;

import android.app.Fragment;
import android.os.Bundle;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.messenger.inwady.myssenger.R;
import com.messenger.inwady.myssenger.model.App;

/* MAKE INTERFACE @ FOR App.Screen */

public class Settings extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = null;
        try {
            view = inflater.inflate(R.layout.screen_settings,
                    container, false);

            App.now = App.Screen.settings;

        }catch (InflateException e) {}

        return view;
    }

}
