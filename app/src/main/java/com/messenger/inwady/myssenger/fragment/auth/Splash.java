package com.messenger.inwady.myssenger.fragment.auth;

import android.app.Fragment;
import android.os.Bundle;
import android.os.Message;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.messenger.inwady.myssenger.R;
import com.messenger.inwady.myssenger.ui.CircleProgress;

public class Splash extends Fragment {

    RelativeLayout rMain = null;
    CircleProgress progress = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.screen_splash,  container, false);

        rMain = (RelativeLayout) view.findViewById(R.id.splash_layout);
        progress = new CircleProgress(getActivity(), rMain);

        RelativeLayout.LayoutParams rlp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT);
        rlp.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
        progress.setLayoutParams(rlp);

        rMain.addView(progress);

        return view;
    }

}
