package com.messenger.inwady.myssenger.controller;

import android.app.Activity;
import android.util.Log;

import com.messenger.inwady.myssenger.fragment.content.Chats;
import com.messenger.inwady.myssenger.model.utils.Pack;
import com.messenger.inwady.myssenger.model.web.GenerateEvent;
import com.messenger.inwady.myssenger.utils.MD5;

import java.util.HashMap;

public class ContactsController extends MainController {

    public static String action = "contactlist";

    public ContactsController(Activity act, GenerateEvent event) {
        super(act, event);
    }

    @Override
    public void startController() {
        app.mService.runRequest(pack.toJson());
    }

    public void setPack(String sid, String cid) {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("sid", sid);
        map.put("cid", cid);
        pack = Pack.genDefaultPack(action, map);
    }

}