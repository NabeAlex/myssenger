package com.messenger.inwady.myssenger.data;

public class Me extends Person {
    public String sid, cid, nick;

    public Me(String sid, String cid, String nick) {
        super(nick);
        this.sid = sid;
        this.cid = cid;
        this.nick = nick;
    }
}
