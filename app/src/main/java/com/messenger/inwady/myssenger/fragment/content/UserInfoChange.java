package com.messenger.inwady.myssenger.fragment.content;

import android.Manifest;
import android.app.Fragment;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Base64;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.messenger.inwady.myssenger.R;
import com.messenger.inwady.myssenger.controller.UserInfoChangeController;
import com.messenger.inwady.myssenger.controller.UserInfoController;
import com.messenger.inwady.myssenger.data.Resource;
import com.messenger.inwady.myssenger.model.App;
import com.messenger.inwady.myssenger.model.utils.Pack;
import com.messenger.inwady.myssenger.model.web.GenerateEvent;

import java.io.IOException;

import butterknife.Bind;
import butterknife.ButterKnife;

public class UserInfoChange extends Fragment  implements View.OnClickListener {
    public static final int PICTURE = 1;

    public App app = null;
    public Fragment me = null;

    public LayoutInflater inflater = null;
    public void setInflater(LayoutInflater i) {
        inflater = i;
    }

    /*** Controllers ***/

    GenerateEvent user;
    private UserInfoChangeController changeInfoController = null;

    GenerateEvent userInfo;
    private UserInfoController userInfoController = null;

    /*******************/

    private static View view;
    static {
        view = null;
    }

    @Bind(R.id.info_change_nick)
    TextView nickView;
    @Bind(R.id.change_info_status)
    public EditText editStatus;

    @Bind(R.id.change_info_email)
    public EditText editEmail;
    @Bind(R.id.change_info_phone)
    public EditText editPhone;

    @Bind(R.id.change_info_attach)
    public Button buttonAttach;

    @Bind(R.id.change_info_button)
    public Button save;

    @Bind(R.id.user_change_image)
    public ImageView imageView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if(view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        } else
            view = inflater.inflate(R.layout.screen_user_info_change,
                    container, false);

        ButterKnife.bind(this, view);

        app = App.getInstance();
        App.now = App.Screen.user_info_change;

        nickView.setText(app.getMe().nick);

        me = this;

        setEvents();

        save.setOnClickListener(this);
        buttonAttach.setOnClickListener(this);

        //userInfoController = new UserInfoController(getActivity(), userInfo);
        //userInfoController.setPack(app.sid, app.cid, app.cid /* !!! */);
        //userInfoController.startController();

        changeInfoController = new UserInfoChangeController(getActivity(), user);

        return view;
    }

    private void HandleError(int status) {
        App.Toast(app, "Error!");
    }

    private void setEvents() {
        userInfo = new GenerateEvent(new Handler() {
            public void handleMessage(Message msg) {
                Pack s = Pack.parseMessagePack(msg);

            }
        });

        user = new GenerateEvent(new Handler() {
            public void handleMessage(Message msg) {
                Pack s = Pack.parseMessagePack(msg);
                boolean bool = Pack.statusError(s) == 0;
                if(bool) App.Toast(app, "Success!");
                else App.Toast(app, "Try later.");
            }
        });
    }

    private String value_picture = "";
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.change_info_attach:

                /* requestPermissions */
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (ContextCompat.checkSelfPermission(getActivity(),
                            Manifest.permission.READ_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED) {

                            ActivityCompat.requestPermissions(getActivity(),
                                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
                            return;


                    }
                }
                /* */

                Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, PICTURE);

                break;
            case R.id.change_info_button:
                String value_status = editStatus.getText().toString();

                String value_email = editEmail.getText().toString();
                String value_phone = editPhone.getText().toString();
                String value_pic = "";
                if(value_picture != null)
                    value_pic = value_picture;

                changeInfoController.setPack(app.getMe().sid, app.getMe().cid,
                                             value_status, value_email, value_phone, value_pic);
                changeInfoController.startController();

                break;
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == UserInfoChange.PICTURE && resultCode == getActivity().RESULT_OK
                                                  && data != null) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = { MediaStore.Images.Media.DATA };

            Cursor cursor = getActivity().getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            Log.d("Picture", picturePath);
            cursor.close();
            Bitmap bitmap = BitmapFactory.decodeFile(picturePath);
            bitmap = Bitmap.createScaledBitmap(bitmap, 64, 64, true);
            imageView.setImageBitmap(bitmap);
            value_picture = Resource.encodeToBase64(bitmap);
        }
    }

}
