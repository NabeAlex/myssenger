package com.messenger.inwady.myssenger.model;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Application;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.preference.PreferenceManager;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.messenger.inwady.myssenger.MessageService;
import com.messenger.inwady.myssenger.controller.AuthController;
import com.messenger.inwady.myssenger.data.DBHelper;
import com.messenger.inwady.myssenger.data.Me;
import com.messenger.inwady.myssenger.data.Resource;
import com.messenger.inwady.myssenger.model.encryption.EncodingString;
import com.messenger.inwady.myssenger.model.utils.Pack;
import com.messenger.inwady.myssenger.model.web.ConnectTask;
import com.messenger.inwady.myssenger.model.web.GenerateEvent;
import com.messenger.inwady.myssenger.model.web.bind.MessageBinder;
import com.messenger.inwady.myssenger.utils.MD5;
import com.messenger.inwady.myssenger.utils.MessengerSharedPrefs;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.Socket;

public class App extends Application {
    public static boolean START_OF = true;
    public static boolean PAUSE_OF = true;

    private static App app = null;

    public static int STATUS_APP; /******* 0 - OK *********/
                                  /***** 1 - OFFLINE *****/

      static {
          STATUS_APP = 0;
      }

    public static String openUID = null;
    public static String openNick = null;

    public static Bitmap userBitmapChat = null;

    public static Router router = new Router();
    public static MessengerSharedPrefs mprefs;

    public static Screen now = null;
    public enum Screen {
        chat, user_info, chats_screen, user_info_change, settings, imports
    }
    public static boolean isMenuByBack() {
        return now == Screen.user_info_change || now == Screen.user_info_change || now == Screen.chat;
    }

    /*** Controllers ***/

    public GenerateEvent lin;
    public GenerateEvent reg;
    public AuthController authController = null;

    /*******************/

    public String login = null;

    private Me me;
    public Me getMe() {
        return me;
    }
    public void setMe(Me me) {
        this.me = me;
    }

    public void saveMe(String login, String password, String nick) {
        App.mprefs.setSharedPrefs(login, MD5.generate(password), nick);
    }

    public void saveMe(String login, String password) {
        App.mprefs.setSharedPrefs(login, MD5.generate(password), me.nick);
    }

    /* ========= */

    public Handler updateChat = null;

    public Socket socket = null;

    public Boolean onSocket = false;

    public void initSocket() {
        ConnectTask connectTask = new ConnectTask();
        connectTask.app = app;
        connectTask.start(null, new HandShake());
    }

    //**** Service's work ****//
    public MessageService mService = null;

    private boolean Bound = false;

    public boolean getWorkService() {
        return  Bound;
    }

    public void setWorkService(boolean status) {
        Bound = status;
    }

    public ServiceConnection mConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder service) {
            MessageBinder mb = (MessageBinder) service;
            Log.d("Test", "StartSocket");
            mService = mb.getService();
            initSocket();
            Bound = true;
        }

        public void onServiceDisconnected(ComponentName className) {
            Bound = false;
            Log.d("Destroy", app.getWorkService() + "");
            /*
            if(app.getWorkService()) {
                app.mService.destroySocket();
                app.setWorkService(false);
            }
            */
        }
    };;

    //************************//

    @Override
    public void onCreate() {
        if (app != null) return;
        else app = this;

        super.onCreate();
        Resource.initResource(this);
        EncodingString.setKey(Resource.SectetKey);

        SharedPreferences p = PreferenceManager.getDefaultSharedPreferences(this);
        mprefs = new MessengerSharedPrefs(p, p.edit());

        Intent intent = new Intent(this, MessageService.class);

        if(!ServiceIsRunning(MessageService.class)) {
            startService(intent);
        }

        bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }

    public static App getInstance(){
        return app;
    }
    public static DBHelper getDataBase() { return getInstance().mService.getDataBase(); }

    private String lastS = null;
    private JSONObject lastJ = null;
    private String lastA = null;

    public class HandShake extends Handler {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            onSocket = true;

            mService.setSocket(socket);
            mService.setHandlerReceiver(new Handler() {
                public void handleMessage(Message msg) {
                    if(START_OF) {
                        START_OF = false;
                        /* OFFLINE */
                        SharedPreferences p = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
                        mprefs = new MessengerSharedPrefs(p, p.edit());
                        Log.d("!!!", mprefs.getLogin());
                        authController = new AuthController(new GenerateEvent(new Handler() {
                            public void handleMessage(Message msg) {
                                Pack pack = Pack.parseMessagePack(msg);

                                setMe(new Me(pack.data.get("sid"), pack.data.get("cid"), pack.data.get("nick")));
                            }
                        }));
                        authController.setMessageService(mService);
                        authController.setPack(mprefs.getLogin(), mprefs.getPassword(), false);
                        authController.startController();
                    }
                    lastS = Pack.parseMessageJson(msg);
                    try {
                        lastJ = new JSONObject(lastS);
                        lastA = lastJ.getString("action");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    try {
                        if(Pack.statusError(lastJ.getJSONObject("data")) > 0)
                            mService.initSocketWithService();
                    }catch (Exception e) {}

                    router.packEvent(lastA, lastS);
                    }
                }

                );
                mService.runReceiver();
            }
        }

        public void hideKeyboard(Activity act) {
            InputMethodManager inputMethodManager = (InputMethodManager) this.getSystemService(Activity.INPUT_METHOD_SERVICE);
            View view = act.getCurrentFocus();

            if(view == null) {
                view = new View(this);
            }
            inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }

        public void SnackBar(CoordinatorLayout c, String text) {
            Snackbar.make(c, text, Snackbar.LENGTH_SHORT).show();
        }

        public void Toast(String text) {
            Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT).show();
        }

        public static void Toast(Context act, String text) {
            Toast.makeText(act, text, Toast.LENGTH_SHORT).show();
        }

        private boolean ServiceIsRunning(Class<?> serviceClass) {
            ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
            for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
                if (serviceClass.getName().equals(service.service.getClassName())) {
                    return true;
                }
            }
            return false;
        }
}
