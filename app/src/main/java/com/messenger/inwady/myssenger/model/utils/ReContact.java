package com.messenger.inwady.myssenger.model.utils;


public class ReContact {
    public String name, email, phone;

    private String uid = null;
    public String getUid() {
        return uid;
    }

    public ReContact(String uid, String name, String email, String phone) {
        this.uid = uid;
        this.name = name;
        this.email = email;
        this.phone = phone;
    }
}
