package com.messenger.inwady.myssenger.controller;

import android.app.Activity;
import android.app.AlertDialog;
import android.util.Log;

import com.messenger.inwady.myssenger.MessageService;
import com.messenger.inwady.myssenger.data.Me;
import com.messenger.inwady.myssenger.model.App;
import com.messenger.inwady.myssenger.model.utils.Pack;
import com.messenger.inwady.myssenger.model.web.GenerateEvent;
import com.messenger.inwady.myssenger.utils.MD5;

import java.util.HashMap;

public class AuthController extends MainController {
    private MessageService ms = null;
    public void setMessageService(MessageService m) {
        ms = m;
    }

    public void startController() {
        link.putEvent(pack.action, event.getPost());
        event.getBegin().handleMessage(null);
        if(app != null) {
            app.mService.setHandlerRequest(event.getPre());
            app.mService.runRequest(pack.toJson());
        } else {
            ms.setHandlerRequest(event.getPre());
            ms.runRequest(pack.toJson());
        }
    }

    private static String[] receiveKeys = new String[10];
    static {
        receiveKeys[0] = "sid";
        receiveKeys[1] = "cid";
        receiveKeys[2] = "nick";
    }

    public static String action = "auth";
    public static String actionRegister = "register";

    public AuthController(Activity act, GenerateEvent event, Pack pack) {
        super(act, event, pack);
    }

    public AuthController(Activity act, GenerateEvent event) {
        super(act, event);
    }

    public AuthController(GenerateEvent event) {
        super(event);
    }

    public void setPack(String login, String pass) {
        String md5 = MD5.generate(pass);
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("login", login);
        map.put("pass", md5);
        pack = Pack.genDefaultPack(AuthController.action, map);
    }

    public void setPack(String login, String pass, String nick) {
        String md5 = MD5.generate(pass);
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("login", login);
        map.put("pass", md5);
        map.put("nick", nick);
        pack = Pack.genDefaultPack(actionRegister, map);
    }

    public void setPack(String login, String pass, boolean md5_bool) {
        if(md5_bool) setPack(login, pass);
        else {
            HashMap<String, String> map = new HashMap<String, String>();
            map.put("login", login);
            map.put("pass", pass);
            pack = Pack.genDefaultPack(action, map);
        }
    }

    public static Me getPack(Pack pack) {
        return new Me(pack.data.get("sid"),
                      pack.data.get("cid"),
                      pack.data.get("nick"));
    }
}
