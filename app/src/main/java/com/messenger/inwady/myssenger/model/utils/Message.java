package com.messenger.inwady.myssenger.model.utils;

import android.graphics.Bitmap;

public class Message {
    public Bitmap image = null;
    public Message setImage(Bitmap bitmap) {
        image = bitmap;
        return this;
    }

    public boolean my;
    public String user;
    public String message;

    private String uid;
    public String getUser() {
        return  uid;
    }

    public Message(boolean my, String User, String message) {
        this.my = my;
        this.message = message;
        this.user = User;
        this.uid = null;
    }

    public Message(boolean my, String User, String message, String uid) {
        this.my = my;
        this.message = message;
        this.user = User;
        this.uid = uid;
    }
}