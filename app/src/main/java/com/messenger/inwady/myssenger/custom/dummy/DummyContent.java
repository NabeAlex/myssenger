package com.messenger.inwady.myssenger.custom.dummy;

import android.graphics.Bitmap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DummyContent {

    public static List<DummyItem> ITEMS = new ArrayList<DummyItem>();
    public static void addItem(DummyItem item) {
        ITEMS.add(item);
    }
    public static void clearItems() {
        ITEMS.clear();
    }

    public static List<LeftDummyItem> LEFT_ITEMS = new ArrayList<LeftDummyItem>();
    public static void addLeftItem(LeftDummyItem item) {
        LEFT_ITEMS.add(item);
    }
    public static void clearLeftItems() {
        LEFT_ITEMS.clear();
    }

    public static class DummyItem extends LeftDummyItem {
        public Bitmap bitmap = null;

        public String name = null;
        public String desc = null;


        public int online = 0;

        public DummyItem setChatId(String chid) {
            this.uid = chid;
            return this;
        }
        public String getChatId() {
            return uid;
        }

        public DummyItem setNoRead(int read) {
            noRead = read;
            return this;
        }

        public DummyItem(String id, String name, int online, String desc) {
            super(id, 0);
            this.name = name;
            this.desc = desc;
            this.online = online;
        }

        public DummyItem(String id, String name, String desc, int online, Bitmap bitmap) {
            super(id, 0);
            this.name = name;
            this.desc = desc;
            this.online = online;
            this.bitmap = bitmap;
        }

        public DummyItem(String id, String name, String desc, Bitmap bitmap) {
            super(id);
            this.name = name;
            this.desc = desc;
            this.bitmap = bitmap;
        }

        @Override
        public String toString() {
            return name;
        }
    }

    public static class LeftDummyItem {
        public String uid = null;
        public int noRead = 0;

        public LeftDummyItem(String uid) {
            this.uid = uid;
        }

        public LeftDummyItem(String uid, int noRead) {
            this.uid = uid;
            this.noRead = noRead;
        }
    }
}
