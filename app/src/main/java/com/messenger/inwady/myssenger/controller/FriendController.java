package com.messenger.inwady.myssenger.controller;

import android.app.Activity;
import android.os.Handler;
import android.util.Log;

import com.messenger.inwady.myssenger.data.Me;
import com.messenger.inwady.myssenger.model.App;
import com.messenger.inwady.myssenger.model.utils.Pack;
import com.messenger.inwady.myssenger.model.utils.ReContact;
import com.messenger.inwady.myssenger.model.web.GenerateEvent;

import java.util.ArrayList;
import java.util.HashMap;

public class FriendController {
    public enum Friend {
        DELETE {
            public String getAction() { return  "delcontact"; }
        }, ADD {
            public String getAction() { return "addcontact"; }
        };

        public String getAction() { return ""; }
    }

    Me me = null;
    private ArrayList<ReContact> re = null;
    private Friend friend;

    private GenerateEvent evendFriend = new GenerateEvent(new Handler() {
        public void handleMessage(android.os.Message msg) {
            Log.d("!", "!");

            if(re.size() > 0)
                setFriend(friend, me.sid, me.cid, re.get(0).getUid());
            else return;

            re.remove(0);
        }
    });

    public FriendController(Me me, Friend friend, ArrayList<ReContact> re) {
        this.me = me;
        this.re = re;
        this.friend = friend;
    }

    public void start() {
        evendFriend.getPost().handleMessage(null);
    }

    private void setFriend(Friend friend, String sid, String cid, String uid) {
        App.router.putEvent(friend.getAction(), evendFriend.getPost());

        App.getInstance().mService.setHandlerRequest(null);
        Pack pack = new Pack();
        pack.action = friend.getAction();
        pack.data.put("sid", sid);
        pack.data.put("cid", cid);
        pack.data.put("uid", uid);
        App.getInstance().mService.runRequest(pack.toJson());
    }
}
