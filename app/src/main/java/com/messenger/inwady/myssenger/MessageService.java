package com.messenger.inwady.myssenger;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.messenger.inwady.myssenger.controller.EventsController;
import com.messenger.inwady.myssenger.custom.ChatsFragment;
import com.messenger.inwady.myssenger.data.DBHelper;
import com.messenger.inwady.myssenger.data.Resource;
import com.messenger.inwady.myssenger.fragment.content.ChatField;
import com.messenger.inwady.myssenger.model.App;
import com.messenger.inwady.myssenger.model.utils.Pack;
import com.messenger.inwady.myssenger.model.utils.ReContact;
import com.messenger.inwady.myssenger.model.web.ConnectTask;
import com.messenger.inwady.myssenger.model.web.GenerateEvent;
import com.messenger.inwady.myssenger.model.web.Receiver;
import com.messenger.inwady.myssenger.model.web.Request;
import com.messenger.inwady.myssenger.model.web.bind.MessageBinder;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.HashMap;

public class MessageService extends Service {
    public static boolean IS_APP;
    static {
        IS_APP = true;
    }

    private App app;
    private DBHelper db;

    /*** Controllers ***/

    GenerateEvent eventEvent; /* ^_^ */
    private EventsController eventConroller = null;

    /*******************/

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("SocketService", "onStartCommand");
        return START_STICKY;
    }

    public DBHelper getDataBase() {
        return db;
    }

    private Socket socket = null;

    public void closeAll() throws IOException {
        socket.close();
    }

    public void initSocketWithService() {
        app.onSocket = false;
        destroySocket();
        ConnectTask connectTask = new ConnectTask();
        connectTask.app = app;
        connectTask.start(null, new HandShake());
    }

    public void destroySocket() {
        app.socket = null;
        receiver.destroyTask();
        request.destroyTask();
    }

    public class HandShake extends Handler {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            app.onSocket = true;
            receiver.setSocket(socket);
            Log.d("App.now", App.now + "");
            if(App.now != null) {
                app.authController.setPack(app.login, app.mprefs.getPassword(), false);
                app.authController.setEvent(new GenerateEvent(new Handler() {
                     public void handleMessage(Message msg) {
                        // OK or TO (AUTH)
                     }
                }));
                app.authController.startController();
            }

        }
    }

    public void setSocket(Socket s) {
        socket = s;
        app.socket = s;
    }

    ///**** Receiver ****///

    private Receiver receiver = null;
    private Handler receiver_handler = null;

    public void setHandlerReceiver(Handler handler) {
        receiver_handler = handler;
    }

    public void runReceiver() {
        if(receiver == null) receiver = new Receiver();
        receiver.start(socket, receiver_handler);
    }

    ///**** Request  ****///

    private Request request = new Request();
    private Handler request_handler;
    private String message = null;

    public void setHandlerRequest(Handler handler) { request_handler = handler; }
    public void setMessage(String m) { message = m; }
    public void runRequest(String message) {
        request.start(socket, request_handler, message);
    }
    ///******************///

    private final IBinder mBinder = new MessageBinder(this);

    @Override
    public IBinder onBind(Intent intent) {
        app = App.getInstance();

        /* INIT DB */
        db = new DBHelper(app);
        /***********/

        setEvents();
        eventConroller = new EventsController(eventEvent);
        eventConroller.startController();

        return mBinder;
    }

    private HashMap<String, Integer> userMap = null;

    class Notify {
        public String uid = null;

        public int id;
        public String message;
        public CharSequence charSequence;

        public Notify(int id, String uid, String message) {
            this.id = id;
            this.uid = uid;
            this.message = message;
            charSequence = new CharSequence() {
                @Override
                public int length() {
                    return 0;
                }

                @Override
                public char charAt(int index) {
                    return 0;
                }

                @Override
                public CharSequence subSequence(int start, int end) {
                    return null;
                }
            };
            charSequence = "s";
        }
    }

    public void sendNotification(String nick, Notify notify, Bitmap bitmap) {
        if(userMap == null)
            userMap = new HashMap<>();

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context
                .NOTIFICATION_SERVICE);


        final CharSequence chars = notify.message;

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setSmallIcon(R.drawable.user_image)
                .setVibrate(new long[] {1, 1, 1})
                .setCategory(NotificationCompat.CATEGORY_MESSAGE)
                .setContentTitle(nick)
                .setContentText(notify.message);

        if(bitmap != null)
            notificationBuilder.setLargeIcon(bitmap);


        Intent notificationIntent = new Intent(this, Login.class);
        notificationIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
                Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);

        notificationIntent.putExtra("uid", notify.uid);
        if(nick != null) {
            notificationIntent.putExtra("nick", nick);
        }

        PendingIntent contentIntent = PendingIntent.getActivity(this,
                0, notificationIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        notificationBuilder.setContentIntent(contentIntent);
        notificationManager.notify(0, notificationBuilder.build());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            closeAll();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Log.d("Service", "onDestroy");
    }

    private void setEvents() {
        eventEvent = new GenerateEvent(new Handler() {
            public void handleMessage(android.os.Message msg) {
                Pack pack = new Pack(Pack.parseMessageJson(msg));

                final ChatsFragment chats = Main.getChats();
                final ChatField chatField = Main.getChatField();

                String from = pack.data.get("from");
                String mime = "", bitmap = "";
                try {
                    JSONObject jsonObject = new JSONObject(pack.data.get("attach"));
                    /* WITH OUT */
                    mime = jsonObject.getString("mime");
                    bitmap = jsonObject.getString("data");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                boolean my;

                if(app.getMe().cid.equals(from)) {
                    Log.d("RECEIVE", "MESSAGE");

                    my = true;

                    try {
                        if(db.isSet(chatField.chid)) {
                            db.insertToChat(chatField.chid, from, pack.data.get("body"), Integer.parseInt(pack.data.get("time")),
                                            mime, bitmap);
                        } else {
                            db.insertUser(new ReContact(chatField.chid, "", "", ""), "no_friend", "");
                            db.insertToChat(chatField.chid, from, pack.data.get("body"), Integer.parseInt(pack.data.get("time")),
                                            mime, bitmap);
                        }

                        try {
                            chats.showLocalMyMessages(true);
                        } catch (Exception e) {

                        }

                    } catch (Exception e) {
                        Log.d("App is down!", "chatField");
                    }
                    //chatAdapter.add(EventsController.genAnswer(pack.action, app.getMe().nick.equals(pack.data.get("nick")),
                    //        pack.data.get("nick"), body, pack));
                }  else {
                    Log.d("RECEIVE", "NO MY MESSAGE");

                    Log.d("!!!!", "" + db.isSet(from) + db.isFriend(from) + (App.now == App.Screen.chat));

                    //sendNotification(pack.data.get("nick"), new Notify(0, from, pack.data.get("body")), null);
                    if(db.isSet(from)) {
                        db.insertToChat(from, from, pack.data.get("body"), Integer.parseInt(pack.data.get("time")), mime,
                                        bitmap);

                        if((App.now != null) && App.now == App.Screen.chat) {
                            if(!chatField.chid.equals(from)) {
                                db.upCountRead(from);
                            }
                        } else {
                            db.upCountRead(from);
                        }

                        try {
                            Log.d("Notyfy", (!App.START_OF) + "");
                            if (App.PAUSE_OF || App.START_OF)
                                sendNotification(pack.data.get("nick"), new Notify(0, from, pack.data.get("body")), null);
                            else
                                chats.showLocalMyMessages(true);
                        } catch (Exception e) {
                            sendNotification(pack.data.get("nick"), new Notify(0, from, pack.data.get("body")), null);
                        }

                    } else {
                        db.insertUser(new ReContact(from, "", "", ""), "no_friend", "");
                        db.insertToChat(from, from, pack.data.get("body"), Integer.parseInt(pack.data.get("time")), mime, bitmap);
                        db.upCountRead(from);

                        try {
                            Log.d("Notyfy", (!App.START_OF) + "");
                            if(App.PAUSE_OF || App.START_OF)
                                sendNotification(pack.data.get("nick"), new Notify(0, from, pack.data.get("body")), null);
                            else
                                chats.showLocalLeftMessages(true);

                        } catch (Exception e) {
                            sendNotification(pack.data.get("nick"), new Notify(0, from, pack.data.get("body")), null);
                        }
                    }

                    my = false;

                    //chatAdapter.add(EventsController.genAnswer(pack.action, true,
                    //        pack.data.get("nick"), body, pack));
                }

                try {
                    if(from.equals(chatField.chid) || my) {
                        chatField.addMessage(new com.messenger.inwady.myssenger.model.utils.Message(my, from, pack.data.get("body"))
                                                 .setImage(Resource.decodeToBase64(bitmap)));
                    }
                } catch (Exception e) {}

                //if(EventsController.my_nickname != null) app.getMe().nick = EventsController.my_nickname;
            }
        });
    }

}
