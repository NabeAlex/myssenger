package com.messenger.inwady.myssenger.model.encryption;

public class EncodingString {
    private static byte[] key = null;

    public static String encode(String victim) {
        if(key == null) key = "".getBytes();
        byte[] in = victim.getBytes();
        byte[] out = new byte[victim.length()];
        for(int i = 0; i < victim.length(); i++) {
            out[i] = (byte) (in[i] ^ key[i % key.length]);
        }
        return new String(out);
    }

    public static String decode(String bye_victim) {
        return encode(bye_victim);
    }

    public static void setKey(String key) { EncodingString.key = key.getBytes(); }
}