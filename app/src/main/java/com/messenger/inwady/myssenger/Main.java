package com.messenger.inwady.myssenger;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.messenger.inwady.myssenger.controller.AuthController;
import com.messenger.inwady.myssenger.controller.EventsController;
import com.messenger.inwady.myssenger.controller.FriendController;
import com.messenger.inwady.myssenger.custom.ChatsFragment;
import com.messenger.inwady.myssenger.data.DBHelper;
import com.messenger.inwady.myssenger.fragment.auth.Splash;
import com.messenger.inwady.myssenger.fragment.content.ChatField;
import com.messenger.inwady.myssenger.fragment.content.Chats;
import com.messenger.inwady.myssenger.fragment.content.Contacts;
import com.messenger.inwady.myssenger.fragment.content.Settings;
import com.messenger.inwady.myssenger.fragment.content.UserInfo;
import com.messenger.inwady.myssenger.fragment.content.UserInfoChange;
import com.messenger.inwady.myssenger.model.App;
import com.messenger.inwady.myssenger.model.Router;
import com.messenger.inwady.myssenger.model.utils.Pack;
import com.messenger.inwady.myssenger.model.utils.ReContact;
import com.messenger.inwady.myssenger.model.web.GenerateEvent;
import com.messenger.inwady.myssenger.model.web.bind.MessageBinder;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Main extends AppCompatActivity {
    public App app = null;
    private DBHelper db = null;

    FragmentManager fm = null;

    private static ChatsFragment chats;
    public static ChatsFragment getChats() {
        return chats;
    }
    public static void setChats(ChatsFragment chats) {
        Main.chats = chats;
    }

    private static ChatField chatField;
    public static ChatField getChatField() {
        return chatField;
    }

    private UserInfo userInfo;

    ListView drawer = null;
    ArrayAdapter<String> adapter = null;
    List<String> array = null;

    private ActionBarDrawerToggle mDrawerToggle;

    DrawerLayout drawerLayout = null;
    Toolbar toolbar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        app = App.getInstance();
        try {
            db = App.getDataBase();
        } catch (Exception e) {

            e.printStackTrace();
        }

        App.now = App.Screen.chats_screen;


        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);

        fm = getFragmentManager();
        fm.beginTransaction().replace(R.id.main_frag, new Chats()).commit();

        setMenu();
        drawer = (ListView)findViewById(R.id.left_menu_list);
        adapter = new ArrayAdapter<String>(this, R.layout.menu_list_item, array);
        drawer.setAdapter(adapter);

        drawer.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView a, View v, int pos, long l) {
                clickMenu(pos + 1);
            }

        });

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        setDrawerState(true);

    }

    @Override
    public void onStart() {
        super.onStart();

        App.PAUSE_OF = false;

        Log.d("Enter from App", "UWW");
    }

    @Override
    public void onPause() {
        super.onPause();

        App.PAUSE_OF = true;

        Log.d("Exit from App", "UWW");
    }


    Fragment f;
    public boolean updateFragment(App.Screen e) {
        boolean root = App.now == App.Screen.chats_screen;
        boolean main = false;

        switch (e) {
            case chats_screen:
                main = true;
                App.now = App.Screen.chats_screen;
                f = new Chats();
                break;
            case user_info_change:
                App.now = App.Screen.user_info_change;
                f = new UserInfoChange();
                break;
            case user_info:
                App.now = App.Screen.user_info;
                f = new UserInfo();
                break;
            case settings:
                App.now = App.Screen.settings;
                f = new Settings();
                break;
            default: return false;
        }
        if(root) {
            fm.beginTransaction()
                    .replace(R.id.main_frag, f).addToBackStack("main")
                    .commit();
        }else {
            fm.beginTransaction()
                    .replace(R.id.main_frag, f).addToBackStack("menu")
                    .commit();
        }
        if(main) {
            setMenuState(R.menu.main);
            setDrawerState(true);
        }
        else {
            setMenuState(R.menu.empty);
            setDrawerState(false);
        }
        return true;
    }

    public boolean updateChatFragment(Fragment fragment) {
        if(fragment instanceof ChatField || fragment instanceof UserInfo) {
            if(fragment instanceof ChatField) {
                chatField = (ChatField)fragment;
                setDrawerState(false);
                setMenuState(R.menu.empty);
            }

            if(fragment instanceof UserInfo) {
                userInfo = (UserInfo) fragment;
            }
            fm.beginTransaction()
                    .replace(R.id.main_frag, fragment)
                    .addToBackStack("chat")
                    .commit();
        } else return false;

        return true;
    }


    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("Destroy Main", "!!!!");
    }

    @Override
    public void onBackPressed()
    {
        if(App.now == App.Screen.imports) {
            importAll();
            return;
        }
        if(App.isMenuByBack()) {
            setDrawerState(true);
            setMenuState(R.menu.main);
        }

        if (fm.getBackStackEntryCount() > 0){
            fm.popBackStack();
        }
        else super.onBackPressed();
    }

    private static int menu_;
    static { menu_ = R.menu.main; }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(menu_, menu);
        return true;
    }

    private static ArrayList<ReContact> reContacts = null;
    public static void setContacts(ArrayList<ReContact> re) {
        reContacts = re;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                setDrawerState(false);

                fm.beginTransaction()
                        .replace(R.id.main_frag, new Contacts()).addToBackStack("main")
                        .commit();

                setMenuState(R.menu.contact);
                break;
            case R.id.action_logout:
                app.mprefs.clear();
                db.deleteTable();
                
                App.now = null;

                Intent intent = new Intent(this, Login.class);
                intent.setFlags(Intent.FILL_IN_ACTION | Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(intent);

                break;
            case R.id.action_accept:
                importAll();
                break;

            case R.id.action_refresh:

                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void importAll() {
        if(reContacts != null)
            if(reContacts.size() > 0)
                new FriendController(app.getMe(), FriendController.Friend.ADD, reContacts).start();
        setDrawerState(true);
        setMenuState(R.menu.main);
        fm.popBackStack();
    }

    private void setMenu() {
        array = new ArrayList<String>();
        /* 1 */ /* array.add(getString(R.string.left_menu_1)); */
        /* 2 */ array.add(getString(R.string.left_menu_2));
    }

    private  void clickMenu(int index) {
        /*if(index == 1) {
            if(App.now == App.Screen.settings) return;
            drawerLayout.closeDrawers();
            updateFragment(App.Screen.settings);
        }else */ if(index == 1) {
            if(App.now == App.Screen.user_info_change) return;
            drawerLayout.closeDrawers();
            updateFragment(App.Screen.user_info_change);
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newc) {
        super.onConfigurationChanged(newc);
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString(null, null);
        super.onSaveInstanceState(outState);
    }

    public void setDrawerState(boolean isEnable) {
        if (isEnable) {
            drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);

            mDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout,
                    toolbar, R.string.drawer_open, R.string.drawer_close) {

                public void onDrawerClosed(View view) {
                    super.onDrawerClosed(view);
                    setMenuState(R.menu.main);
                }

                public void onDrawerOpened(View drawerView) {
                    super.onDrawerOpened(drawerView);
                    setMenuState(R.menu.empty);
                }
            };


            drawerLayout.setDrawerListener(mDrawerToggle);
            drawerLayout.setScrimColor(Color.TRANSPARENT);

            mDrawerToggle.syncState();

        } else {
            drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
            mDrawerToggle.setDrawerIndicatorEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            mDrawerToggle.setToolbarNavigationClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }
    }

    public void setMenuState(int i) {
        menu_ = i;
        invalidateOptionsMenu();
    }

    public void setToolbar(String string) {
        toolbar.setTitle(string);
    }

}
