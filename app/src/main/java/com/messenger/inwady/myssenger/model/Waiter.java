package com.messenger.inwady.myssenger.model;

import android.os.AsyncTask;

public class Waiter extends AsyncTask<Void,Void,Void> {
    private int mSecs;
    private long timeStart;
    private boolean done = false;
    private WaiterFailed waiterFailed = null;
    private WaiterDone waiterDone = null;

    public Waiter(WaiterFailed wf){
        waiterFailed = wf;
    }

    public Waiter(WaiterFailed wf, WaiterDone wd){
        waiterFailed = wf;
        waiterDone = wd;
    }

    @Override
    protected Void doInBackground(Void... params) {
        while (!done && (System.currentTimeMillis() - timeStart <= mSecs)){
        }
        if (waiterDone != null)
            waiterDone.doIfDone();
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        if (!done){
            failed();
        }
    }

    public void startWaiting(Integer mSecstw){
        mSecs = mSecstw;
        timeStart = System.currentTimeMillis();
        this.execute();
    }

    public void setDone(){
        done = true;
    }

    protected void failed(){
        if (waiterFailed != null)
            waiterFailed.doIfFailed();
    }

    public interface WaiterFailed{
        void doIfFailed();
    }

    public interface WaiterDone{
        void doIfDone();
    }
}
