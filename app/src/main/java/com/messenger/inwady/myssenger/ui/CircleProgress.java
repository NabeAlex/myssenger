package com.messenger.inwady.myssenger.ui;

import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnticipateOvershootInterpolator;
import android.widget.RelativeLayout;

import com.messenger.inwady.myssenger.R;

public class CircleProgress extends View {
    private CircleProgress link = null;
    private RelativeLayout relative = null;

    private boolean gotHandler = false;

    private Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            Log.d("!!!", "" + r);

            x = msg.getData().getInt("x") / 2;
            y = msg.getData().getInt("y") / 2;
            r = MIN_RADIUS;
            fix_x = x;

            range = x / 4; // TEST

            invalidate();

            AnticipateOvershootInterpolator interpolator = new AnticipateOvershootInterpolator(1f);

            ValueAnimator valueAnimator = ValueAnimator.ofInt(-range, range);
            valueAnimator.setDuration(700);
            valueAnimator.setInterpolator(interpolator);
            valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                public void onAnimationUpdate(ValueAnimator animation) {
                    Integer value = (Integer) animation.getAnimatedValue();
                    x = fix_x + value;
                    if(value <= -range) r = MAX_RADIUS;
                    else if(value >=  range) r = MAX_RADIUS;
                    else r = MIN_RADIUS + (int) (((float) Math.abs(value) / range) * (MAX_RADIUS - MIN_RADIUS));
                    invalidate();
                }
            });
            valueAnimator.setRepeatMode(ValueAnimator.REVERSE);
            valueAnimator.setRepeatCount(ValueAnimator.INFINITE);
            valueAnimator.start();
        }
    };

    private boolean closeHandler() {
        if(gotHandler) return true;
        gotHandler = true;
        return false;
    }

    public Handler getHandler() {
        return (closeHandler()) ? null : handler;
    }

    int MIN_RADIUS = 20;
    int MAX_RADIUS = 40;

    private boolean power = true;
    public CircleProgress(Context context, RelativeLayout relative) {
        super(context);
        setId(R.id.circle_progress);
        this.relative = relative;
        link = this;
    }

    public CircleProgress(Context context, RelativeLayout relative, boolean power) {
        super(context);
        setId(R.id.circle_progress);
        this.relative = relative;
        link = this;
        this.power = power;
    }


    private int range = 0;
    private int fix_x = 0;
    public int x = 0;
    public int y = 0;
    public int r = 0;
    Paint mPaint = new Paint();

    @Override
    public void onDraw(Canvas c)
    {
        mPaint.setARGB(103, 58, 183, 1);
        if(!(x == 0 && y == 0))
            c.drawCircle(x, y, r, mPaint);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        if(power == true && hasFocus == true) {
            start();
        }
    }

    public void start() {
        Bundle bundle = new Bundle();
        bundle.putInt("x", relative.getWidth());
        bundle.putInt("y", relative.getHeight());
        Message m = new Message();
        m.setData(bundle);
        handler.handleMessage(m);
    }
}
