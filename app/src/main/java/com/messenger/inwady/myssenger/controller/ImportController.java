package com.messenger.inwady.myssenger.controller;


import android.app.Activity;
import android.content.ContentResolver;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.provider.ContactsContract.Contacts;
import android.util.Log;
import android.widget.ListView;

import com.messenger.inwady.myssenger.model.App;
import com.messenger.inwady.myssenger.model.utils.Contact;
import com.messenger.inwady.myssenger.model.utils.Pack;
import com.messenger.inwady.myssenger.model.utils.ReContact;
import com.messenger.inwady.myssenger.model.web.GenerateEvent;
import com.messenger.inwady.myssenger.utils.JSON;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Vector;

class ContactsInfo {
    public String names[];
    public String emails[];
    public String phones[];

    private int it = 0;
    public int length;

    public ReContact next() {
        ReContact c = new ReContact("", names[it], emails[it], phones[it]);
        it++;
        return c;
    }

    public boolean hasNext() {
        return it < length;
    }

    public ContactsInfo(String[] n, String[] e, String[] p) {
        length = n.length; // EXAMPLE

        names = n;
        emails = e;
        phones = p;
    }
}

public class ImportController extends MainController {

    public static String action = "import";

    /*** Access to contacts ***/

    Cursor contacts;

    /**************************/

    public ImportController(Activity act) { super(act); }
    public ImportController(Activity act, GenerateEvent event) {
        super(act, event);
    }

    public void setPack(ContactsInfo contacts) {
        ReContact re;
        JSONArray jsonArray = new JSONArray();
        JSONObject jsonObject;
        String uid = App.getInstance().getMe().cid;
        while (contacts.hasNext()) {
            re = contacts.next();
            jsonObject = new JSONObject();
            try {
                jsonObject.put("myid", uid);
                jsonObject.put("name", re.name);
                jsonObject.put("email", re.email);
                jsonObject.put("phone", re.phone);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            jsonArray.put(jsonObject);
        }
        JSONObject base = new JSONObject();
        try {
            base.put("contacts", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        pack = Pack.genDefaultPack(action, base);
    }

    public void setPack(ReContact re) {
        JSONArray jsonArray = new JSONArray();
        JSONObject jsonObject;
        String uid = App.getInstance().getMe().cid;

        jsonObject = new JSONObject();
        try {
            jsonObject.put("myid", uid);
            jsonObject.put("name", re.name);
            jsonObject.put("email", re.email);
            jsonObject.put("phone", re.phone);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        jsonArray.put(jsonObject);

        JSONObject base = new JSONObject();
        try {
            base.put("contacts", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        pack = Pack.genDefaultPack(action, base);
    }

    static public ContactsInfo refreshContacts(Activity act) {
        String names_[] = null, emails_[] = null, phones_[] = null;
        try {
            Vector<String> namesVect = new Vector<>();
            Vector<String> emailsVect = new Vector<>();
            Vector<String> phonesVect = new Vector<>();
            final String[] PROJECTION = new String[]{
                    ContactsContract.CommonDataKinds.Email.CONTACT_ID,
                    ContactsContract.Contacts.DISPLAY_NAME,
                    ContactsContract.CommonDataKinds.Email.DATA
            };
            Cursor phones = act.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, null);
            Cursor emails = act.getContentResolver().query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, PROJECTION, null, null, null);
            while (phones.moveToNext()) {
                namesVect.add(phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME)));
                phonesVect.add(phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NORMALIZED_NUMBER)));
                emailsVect.add(phones.getString(emails.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA)));
            }
            phones.close();
            names_ = namesVect.toArray(new String[namesVect.size()]);
            emails_ = emailsVect.toArray(new String[emailsVect.size()]);
            phones_ = phonesVect.toArray(new String[phonesVect.size()]);
        }catch (Exception e) {
            e.printStackTrace();
        }
        return new ContactsInfo(names_, emails_, phones_);
    }

}
