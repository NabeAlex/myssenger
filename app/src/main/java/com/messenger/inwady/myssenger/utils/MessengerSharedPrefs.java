package com.messenger.inwady.myssenger.utils;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Messenger;
import android.provider.SyncStateContract;

import com.messenger.inwady.myssenger.data.Resource;
import com.messenger.inwady.myssenger.model.encryption.EncodingString;

public class MessengerSharedPrefs {
    private SharedPreferences.Editor sEditor;
    private SharedPreferences sPref;

    public MessengerSharedPrefs(SharedPreferences SPREF, SharedPreferences.Editor SEDITOR){
        sEditor = SEDITOR;
        sPref = SPREF;
    }

    public String getLogin(){
        String login = EncodingString.decode(sPref.getString("login", ""));
        return login;
    }

    public String getPassword(){
        String password = sPref.getString("password", "");
        return password;
    }

    public String getNick() {
        String nick = EncodingString.decode(sPref.getString("nick", ""));
        return nick;
    }

    public boolean isValid() {
        return !getLogin().equals("") && !getPassword().equals("");
    }

    public boolean isValidWithNick() {
        return !getLogin().equals("") && !getPassword().equals("") && !getNick().equals("");
    }

    public void setSharedPrefs(String login, String password){
        sEditor.putString("login", EncodingString.encode(login));
        sEditor.putString("password", password);
        sEditor.commit();
    }

    public void setSharedPrefs(String login, String password, String nick){
        sEditor.putString("login", EncodingString.encode(login));
        sEditor.putString("password", password);
        sEditor.putString("nick", EncodingString.encode(nick));
        sEditor.commit();
    }

    public void clear() {
        sEditor.putString("login", "");
        sEditor.putString("password", "");
        sEditor.putString("nick", "");
        sEditor.commit();
    }
}
