package com.messenger.inwady.myssenger.data;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.provider.BaseColumns;
import android.util.Base64;
import android.util.Log;

import com.messenger.inwady.myssenger.R;

import java.io.ByteArrayOutputStream;

public class Resource {
    public static int permCode = 200;

    public static String ip = null;
    public static String port = null;
    public static String full() {
        return ip + ":" + port;
    }

    public static final String DATABASE_NAME = "inwady.sqlite";
    public static final Integer DATABASE_VERSION = 1;

    public static final String  SectetKey = "mypony";

    public static void initResource(Context app) {
        ip = app.getString(R.string.ip);
        port = app.getString(R.string.port);
    }

    public static String encodeToBase64(Bitmap image)
    {
        Bitmap immagex=image;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        immagex.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] b = baos.toByteArray();
        String imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);

        Log.e("LOOK", imageEncoded);

        return imageEncoded;
    }

    public static Bitmap decodeToBase64(String input)
    {
        if(input.equals("")) return null;
        byte[] decodedByte = Base64.decode(input, 0);
        return BitmapFactory.decodeByteArray(decodedByte, 0, decodedByte.length);
    }

    public static String generateQueryCreateTable(String name, String[] keys, String[] values, boolean rewrite) {
        String keyValue = "";
        try {
            int len = Math.min(keys.length, values.length);
            for(int i = 0; i < len; i++) {
                keyValue += keys[i] + " " + values[i] + ((i == len - 1) ? "" : ", ");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        String re = (rewrite) ? "" : "IF NOT EXISTS";
        Log.d("SQL", "CREATE TABLE " + re + " " + name + " (" + keyValue + ");");
        return "CREATE TABLE IF NOT EXISTS " + name + " (" + keyValue + ");";
    }
}
