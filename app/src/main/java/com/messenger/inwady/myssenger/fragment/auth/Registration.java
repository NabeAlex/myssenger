package com.messenger.inwady.myssenger.fragment.auth;

import android.app.Application;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.messenger.inwady.myssenger.Login;
import com.messenger.inwady.myssenger.Main;
import com.messenger.inwady.myssenger.R;
import com.messenger.inwady.myssenger.controller.AuthController;
import com.messenger.inwady.myssenger.data.Me;
import com.messenger.inwady.myssenger.model.App;
import com.messenger.inwady.myssenger.model.Waiter;
import com.messenger.inwady.myssenger.model.utils.Pack;
import com.messenger.inwady.myssenger.model.web.GenerateEvent;

import org.json.JSONException;

public class Registration extends Fragment {
    private App app = null;

    Button regButton;
    public EditText pasEd;
    EditText nicEd;
    public EditText logEd;
    ProgressBar progressBar;
    String login;
    String password;
    public Waiter waiter;


    public Registration(){

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.screen_registration,  container, false);
        setHasOptionsMenu(true);

        app = App.getInstance();

        setEvents();
        app.authController = new AuthController(getActivity(), app.reg);

        regButton = (Button) view.findViewById(R.id.button);
        pasEd = (EditText) view.findViewById(R.id.editText2);
        nicEd = (EditText) view.findViewById(R.id.editText1);
        logEd = (EditText) view.findViewById(R.id.editText);
        progressBar = (ProgressBar) view.findViewById(R.id.regPB);
        progressBar.setVisibility(View.INVISIBLE);
        regButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                password = pasEd.getText().toString();
                login = logEd.getText().toString();
                if ((password.equals("")) || (login.equals("")) || (nicEd.getText().toString().equals(""))) {
                    Toast.makeText(getActivity().getBaseContext(), "Please type login and password", Toast.LENGTH_SHORT);
                } else {
                    progressBar.setVisibility(View.VISIBLE);

                    app.authController.setPack(logEd.getText().toString(), pasEd.getText().toString(), nicEd.getText().toString());
                    app.authController.startController();

                    /* FOR AUTH  */

                    App.router.putEvent("auth", new Handler() {
                        public void handleMessage(Message msg) {
                            String s = Pack.parseMessageJson(msg);
                            Pack pack = new Pack(s);
                            if (Pack.statusError(pack) == 0) {
                                String sid = pack.data.get("sid");
                                String cid = pack.data.get("cid");

                                app.setMe(new Me(sid, cid, pack.data.get("nick")));
                                app.saveMe(logEd.getText().toString(), pasEd.getText().toString(), pack.data.get("nick"));

                                Intent intent = new Intent(App.getInstance(), Main.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                App.getInstance().startActivity(intent);
                                try {
                                    getActivity().finish();
                                } catch (Exception e) {

                                }
                            }
                        }
                    });

                    /************/

                    waiter = new Waiter(new Waiter.WaiterFailed() {
                        @Override
                        public void doIfFailed() {
                            if (getActivity() != null) {
                                Log.d("No connect!", "Auth");
                            }
                                /* RECONNECT */
                        }
                    });
                    waiter.startWaiting(20000);
                }

            }
        });
        return view;
    }

    private void setEvents() {
        app.reg = new GenerateEvent(new Handler() {
            public void handleMessage(Message msg) {

                String s = Pack.parseMessageJson(msg);
                Pack pack = new Pack(s);

            }
        });
    }

}