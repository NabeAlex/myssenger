package com.messenger.inwady.myssenger.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.graphics.Bitmap;
import android.graphics.Picture;
import android.provider.BaseColumns;
import android.util.Log;

import com.messenger.inwady.myssenger.fragment.content.UserInfo;
import com.messenger.inwady.myssenger.model.utils.ReContact;
import com.messenger.inwady.myssenger.utils.MD5;

import java.util.ArrayList;

public class DBHelper extends SQLiteOpenHelper implements BaseColumns {

    public interface TABLES {
        String USERS = "USERS";
        ArrayList<String> USER = new ArrayList<>();
    }

    public interface UsersColumns {
        String UID = "uid";
        String NAME = "name";
        String PHONE = "phone";
        String NO_READ = "no_read";
        String PICTURE = "picture";
        String STATUS = "status";
    }

    public interface UserColumns {
        String FROM = "from_user";
        String MESSAGE = "message";
        String MIMI_TYPE = "mimi_type";
        String IMAGE = "image";
        String TIME = "time";
    }

    private SQLiteDatabase sqlite;
    private SQLiteQueryBuilder qb;

    public DBHelper(Context context) {
        super(context, Resource.DATABASE_NAME, null, Resource.DATABASE_VERSION);
        sqlite = getReadableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.d("SQL", Resource.generateQueryCreateTable(TABLES.USERS, new String[]{
                BaseColumns._ID, UsersColumns.UID,
                UsersColumns.NAME, UsersColumns.PHONE,
                UsersColumns.NO_READ, UsersColumns.PICTURE,
                UsersColumns.STATUS
        }, new String[]{
                "integer primary key autoincrement", "text not null",
                "text not null", "text not null",
                "integer", "text not null",
                "text not null"
        }, true));

        db.execSQL(Resource.generateQueryCreateTable(TABLES.USERS, new String[]{
                BaseColumns._ID, UsersColumns.UID,
                UsersColumns.NAME, UsersColumns.PHONE,
                UsersColumns.NO_READ, UsersColumns.PICTURE,
                UsersColumns.STATUS
        }, new String[]{
                "integer primary key autoincrement", "text not null",
                "text not null", "text not null",
                "integer", "text not null",
                "text not null"
        }, true));

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE " + TABLES.USERS);
        onCreate(getWritableDatabase());
    }

    public void deleteTable() {
        sqlite.execSQL("DROP TABLE " + TABLES.USERS);
        onCreate(getWritableDatabase());
    }

    public Cursor getUsers() {
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        qb.setTables(TABLES.USERS);
        return qb.query(sqlite, new String[]{UsersColumns.UID, UsersColumns.NAME,
                                             UsersColumns.PICTURE, UsersColumns.PHONE,
                                             UsersColumns.STATUS, UsersColumns.NO_READ}, null, null, null, null,
                                             UsersColumns.NO_READ +" DESC");
    }

    public Cursor getChat(String uid) {
        uid = "user_" + MD5.generate(uid);
        // if(!((TABLES.USER != null) && (TABLES.USER.contains(uid)))) return null;
        Log.d("getChat", "hello");

        sqlite = getReadableDatabase();

        qb = new SQLiteQueryBuilder();
        qb.setTables(uid);
        Log.d("DB", uid);
        return qb.query(sqlite, new String[] {UserColumns.FROM,
                                              UserColumns.MESSAGE,
                                              UserColumns.MIMI_TYPE,
                                              UserColumns.IMAGE,
                                              UserColumns.TIME}, null, null, null, null, UserColumns.TIME +" ASC");
    }

    public boolean isSet(String uid) {
        String selectQuery = "SELECT " + BaseColumns._ID + " FROM " + TABLES.USERS +
                                                           " WHERE " + UsersColumns.UID + "='" + uid + "'";
        Cursor cursor = sqlite.rawQuery(selectQuery, null);
        if(cursor.getCount() > 0) return true;
        return false;
    }

    public boolean isFriend(String uid) {
        String selectQuery = "SELECT " + UsersColumns.STATUS + " FROM " + TABLES.USERS +
                " WHERE " + UsersColumns.UID + "='" + uid + "'";
        Cursor cursor = sqlite.rawQuery(selectQuery, null);
        if(cursor.getCount() > 0) {
            cursor.moveToFirst();
            return cursor.getString(cursor.getColumnIndex(UsersColumns.STATUS)).equals("friend");
        }
        return false;
    }

    public Bitmap getUserBitmap(String uid) {
        Cursor c = sqlite.rawQuery("SELECT " + UsersColumns.PICTURE + " FROM " + TABLES.USERS
                                             + " WHERE " + UsersColumns.UID + "='" + uid + "'", null);

        if(c.getCount() > 0) {
            c.moveToFirst();
            return Resource.decodeToBase64(c.getString(c.getColumnIndex(UsersColumns.PICTURE)));
        }
        return null;
    }

    public ArrayList<String> getAllDialogs() {
        ArrayList<String> result = new ArrayList<>();

        Cursor c = sqlite.rawQuery("SELECT name FROM sqlite_master WHERE type='table'", null);

        String tmp;
        if (c.moveToFirst()) {
            while (!c.isAfterLast()) {
                tmp = c.getString(0);

                //if((!tmp.equals(TABLES.USERS))&&(tmp.equals(tmp.toUpperCase())))
                    result.add(tmp);
                c.moveToNext();
            }
        }

        return result;
    }

    public boolean insertUser(ReContact re, String state, String bitmap) {
        ContentValues values = new ContentValues();
        values.put(UsersColumns.NAME, re.name);
        values.put(UsersColumns.UID, re.getUid());
        values.put(UsersColumns.PHONE, re.phone);
        values.put(UsersColumns.NO_READ, 0);
        values.put(UsersColumns.PICTURE, bitmap);
        values.put(UsersColumns.STATUS, state);

        if(isSet(re.getUid())) {
            sqlite.update(TABLES.USERS, values, UsersColumns.UID + " = ?", new String[] { re.getUid() });
        } else {
            Log.d("ReConta", re.getUid());
            sqlite.insert(TABLES.USERS, null, values);

            sqlite.execSQL(Resource.generateQueryCreateTable("user_" + MD5.generate(re.getUid()), new String[]{
                    BaseColumns._ID, UserColumns.FROM, UserColumns.MESSAGE, UserColumns.MIMI_TYPE,
                    UserColumns.IMAGE, UserColumns.TIME
            }, new String[] {
                    "integer primary key autoincrement", "text not null", "text not null",
                    "text not null", "text not null", "integer"
            }, false));

        }
        return true;
    }

    public boolean insertToChat(String table, String who, String message, int time, String MIME, String image) {
        ContentValues values = new ContentValues();

        values.put(UserColumns.FROM, who);
        values.put(UserColumns.MESSAGE, message);
        values.put(UserColumns.TIME, time);
        values.put(UserColumns.MIMI_TYPE, MIME);
        values.put(UserColumns.IMAGE, image);

        table = "user_" + MD5.generate(table);
        sqlite.insert(table, null, values);

        return true;
    }

    public boolean deleteUser(String uid) {
        try {
            String deleteQuery = "DELETE FROM " + TABLES.USERS + " WHERE " + UsersColumns.UID + "='"+ uid +"'";
            Log.d("DELETE", "deleteUser");
            sqlite.execSQL(deleteQuery);

            deleteQuery = "DROP TABLE " + "user_" + MD5.generate(uid);
            Log.d("DELETE TABLE", "deleteUser");
            sqlite.execSQL(deleteQuery);
        } catch (Exception e) {

        }

        return true;
    }

    public void upCountRead(String uid) {
        Log.d("Increase", uid);
        String query = "SELECT " + UsersColumns.NO_READ + " FROM USERS WHERE uid='" + uid + "'";
        Cursor c = sqlite.rawQuery(query, null);

        Integer count = 0;
        while (c.moveToNext()) {
            count = c.getInt(c.getColumnIndex(UsersColumns.NO_READ));
        }
        count += 1;

        query = "UPDATE USERS SET " + UsersColumns.NO_READ  + "=" + count +
                             " WHERE " + UsersColumns.UID + "='" + uid + "'";
        Log.d("LI", query);
        sqlite.execSQL(query);
    }

    public void clearCountRead(String uid) {
        Log.d("Clear", uid);
        String query = "UPDATE USERS SET " + UsersColumns.NO_READ  + "=" + 0 +
                                    " WHERE " + UsersColumns.UID + "='" + uid + "'";
        sqlite.execSQL(query);
    }

    public void closeDB() {
        sqlite.close();
    }

}
