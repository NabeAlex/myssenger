package com.messenger.inwady.myssenger.controller;


import android.app.Activity;

import com.messenger.inwady.myssenger.model.utils.Pack;
import com.messenger.inwady.myssenger.model.web.GenerateEvent;

import java.util.HashMap;

public class SendMessageController extends MainController {
    public static String action = "message";

    public SendMessageController(Activity act, GenerateEvent event) {
        super(act, event);
    }

    public void setPack(String sid, String cid, String uid, String body, String MIME, String attach) {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("sid", sid);
        map.put("cid", cid);
        map.put("uid", uid);
        map.put("body", body);

        String tmp = "'";
        map.put("attach", "{" +
                tmp + "mime" + tmp + ": " + tmp + MIME + tmp + "," +
                tmp + "data" + tmp + ": " + tmp + attach + tmp +
                "}");
        pack = Pack.genDefaultPack(action, map);
    }

}