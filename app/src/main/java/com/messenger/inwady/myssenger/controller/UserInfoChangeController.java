package com.messenger.inwady.myssenger.controller;

import android.app.Activity;
import android.util.Log;

import com.messenger.inwady.myssenger.model.utils.Pack;
import com.messenger.inwady.myssenger.model.web.GenerateEvent;

import java.util.HashMap;

public class UserInfoChangeController extends MainController {
    public static String action = "setuserinfo";

    public UserInfoChangeController(Activity act, GenerateEvent event) {
        super(act, event);
    }

    public void setPack(String sid, String cid, String status,
                        String email, String phone, String picture) {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("sid", sid);
        map.put("cid", cid);
        map.put("user_status", status);
        Log.d(email, phone);
        map.put("email", email);
        map.put("phone", phone);
        String tmp = "\"";
        map.put("picture", picture);
        pack = Pack.genDefaultPack(action, map);
    }

}

