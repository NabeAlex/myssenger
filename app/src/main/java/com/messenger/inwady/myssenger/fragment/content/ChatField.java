package com.messenger.inwady.myssenger.fragment.content;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.DataSetObserver;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.messenger.inwady.myssenger.Main;
import com.messenger.inwady.myssenger.R;
import com.messenger.inwady.myssenger.controller.SendMessageController;
import com.messenger.inwady.myssenger.data.DBHelper;
import com.messenger.inwady.myssenger.data.Resource;
import com.messenger.inwady.myssenger.model.App;
import com.messenger.inwady.myssenger.model.utils.Message;
import com.messenger.inwady.myssenger.model.web.GenerateEvent;
import com.messenger.inwady.myssenger.ui.adapter.MessageArrayAdapter;


public class ChatField extends Fragment implements View.OnClickListener {
    App app = null;
    DBHelper db = null;

    private static int ATTACH_PICTURE = 1;

    private MessageArrayAdapter chatAdapter;

    /*** Controllers ***/

    GenerateEvent sendEvent;
    private SendMessageController sendController = null;


    /*******************/

    private ListView messagesList;
    private EditText textField;

    private Button sendButton = null;
    private Button attachButton = null;

    private ProgressBar progressBar;

    private boolean side = false;

    public String nameChat = null;
    public String chid = null;

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    private static boolean progressRun = true;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        progressRun = true;

        app = App.getInstance();
        db = App.getDataBase();

        Bundle bundle = this.getArguments();
        chid = bundle.getString("chid");
        nameChat = bundle.getString("name");

        db.clearCountRead(chid);

        chatAdapter = new MessageArrayAdapter(getActivity().getApplicationContext(), R.layout.chat_list_item);
        setEvents();

        sendController = new SendMessageController(getActivity(), sendEvent);

    }

    //private static
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.screen_chat, container, false);
        App.now = App.Screen.chat;

        ((Main) getActivity()).setToolbar(nameChat);

        //** TO **//


        sendButton = (Button) view.findViewById(R.id.buttonSend);
        attachButton = (Button) view.findViewById(R.id.attachSend);

        messagesList = (ListView) view.findViewById(R.id.listView1);


        messagesList.setAdapter(chatAdapter);

        messagesList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View v, int position, long l) {
                Message message = (Message) parent.getItemAtPosition(position);
                if (message.getUser() != null) {
                    Bundle b = new Bundle();
                    b.putString("uid", message.getUser());
                    UserInfo info = new UserInfo();
                    info.setArguments(b);
                    ((Main) getActivity()).updateChatFragment(info);
                }
                /*
                Toast:
                    E/rsC++: RS CPP error (masked by previous error): Allocation creation failed
                    E/rsC++: RS CPP error (masked by previous error): Allocation creation failed
                    Fatal signal 11 (SIGSEGV), code 1, fault addr 0x28 in tid 4521 (RenderThread)
                 */
            }
        });

        textField = (EditText) view.findViewById(R.id.chatText);
        progressBar = (ProgressBar) view.findViewById(R.id.progressChatBar);
        progressBar.setVisibility(View.GONE);

        sendButton.setOnClickListener(this);
        attachButton.setOnClickListener(this);

        messagesList.setTranscriptMode(AbsListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);

        chatAdapter.registerDataSetObserver(new DataSetObserver() {
            @Override
            public void onChanged() {
                super.onChanged();
                messagesList.setSelection(chatAdapter.getCount() - 1);
            }
        });

        exportFromBase(chid);

        return view;

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private void setEvents() {
        sendEvent = new GenerateEvent(new Handler() {
            public void handleMessage(android.os.Message msg) {

            }
        }, new Handler() {
            public void handleMessage(android.os.Message msg) {
                textField.setText("");
                sendButton.setEnabled(true);
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.buttonSend:
                if(textField != null && !textField.getText().toString().equals("")) {
                    String body = textField.getText().toString();
                    sendController.setPack(app.getMe().sid, app.getMe().cid, chid, body, "", "");
                    sendController.startController();
                    sendButton.setEnabled(false);
                }
                break;
            case R.id.attachSend:

                /* requestPermissions */
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (ContextCompat.checkSelfPermission(getActivity(),
                            Manifest.permission.READ_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED) {

                        ActivityCompat.requestPermissions(getActivity(),
                                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
                        return;

                    }
                }
                /* */

                final String PICTURE = "Picture";

                final CharSequence attachs[] = new CharSequence[]{ PICTURE };
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("Attach");
                builder.setItems(attachs, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(i, ATTACH_PICTURE);
                    }
                });
                builder.show();
                break;

        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == UserInfoChange.PICTURE && resultCode == getActivity().RESULT_OK
                && data != null) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = { MediaStore.Images.Media.DATA };

            Cursor cursor = getActivity().getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            Log.d("Picture", picturePath);
            cursor.close();
            Bitmap bitmap = BitmapFactory.decodeFile(picturePath);
            bitmap = Bitmap.createScaledBitmap(bitmap, 64, 64, true);

            sendController.setPack(app.getMe().sid, app.getMe().cid, chid, " ", "image", Resource.encodeToBase64(bitmap));
            sendController.startController();
            sendButton.setEnabled(false);
        }
    }

    private void exportFromBase(String uid) {
        Cursor c = db.getChat(uid);
        Log.d("SIZE", c.getCount() + "");
        String image;
        Bitmap b;
        if(c.moveToFirst())
            do {
                Log.d("INDEX", c.getColumnIndex(DBHelper.UserColumns.FROM) + "");
                Log.d("MESSAGE => ", c.getString(c.getColumnIndex(DBHelper.UserColumns.FROM)));
                image = c.getString(c.getColumnIndex(DBHelper.UserColumns.IMAGE));
                b = null;
                if(!image.equals("")) b = Resource.decodeToBase64(image);
                chatAdapter.add(new Message((app.getMe().cid.equals(c.getString(c.getColumnIndex(DBHelper.UserColumns.FROM)))), c.getString(c.getColumnIndex(DBHelper.UserColumns.FROM)),
                        c.getString(c.getColumnIndex(DBHelper.UserColumns.MESSAGE))).setImage(b));
            } while (c.moveToNext());
    }

    public String addMessage(Message message) {
        chatAdapter.add(message);
        return chid;
    }

}