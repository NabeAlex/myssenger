package com.messenger.inwady.myssenger.fragment.content;

import android.app.Fragment;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.messenger.inwady.myssenger.Login;
import com.messenger.inwady.myssenger.Main;
import com.messenger.inwady.myssenger.R;
import com.messenger.inwady.myssenger.controller.ContactsController;
import com.messenger.inwady.myssenger.model.App;
import com.messenger.inwady.myssenger.model.utils.Contact;
import com.messenger.inwady.myssenger.model.web.GenerateEvent;

public class Chats extends Fragment implements View.OnClickListener {
    public App app = null;
    public Fragment me = null;

    public LayoutInflater inflater = null;
    public void setInflater(LayoutInflater i) {
        inflater = i;
    }

    public Fragment chats_list;

    /*** Controllers ***/

    GenerateEvent chats;
    private ContactsController contactsController = null;

    /*******************/

    private static View view;
    static {
        view = null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if(view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }
        try {
            view = inflater.inflate(R.layout.screen_main,
                    container, false);
        }catch (InflateException e) {}

        app = App.getInstance();

        setEvents();
        contactsController = new ContactsController(getActivity(), chats);
        contactsController.setPack(app.getMe().sid, app.getMe().cid);

        Log.d("Chats", "Start");
        if(App.openUID == null)
            contactsController.startController();

        ((Main) getActivity()).setToolbar("Myssenger");

        me = this;

        App.now = App.Screen.chats_screen;


        return view;
    }

    @Override
    public void onClick(View v) {

    }

    private void setEvents() {
        chats = new GenerateEvent(null);
    }
}
