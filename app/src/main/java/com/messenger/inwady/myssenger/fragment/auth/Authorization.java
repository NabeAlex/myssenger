package com.messenger.inwady.myssenger.fragment.auth;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.messenger.inwady.myssenger.Login;
import com.messenger.inwady.myssenger.Main;
import com.messenger.inwady.myssenger.R;
import com.messenger.inwady.myssenger.controller.AuthController;
import com.messenger.inwady.myssenger.data.Me;
import com.messenger.inwady.myssenger.model.App;
import com.messenger.inwady.myssenger.model.Waiter;
import com.messenger.inwady.myssenger.model.utils.Pack;
import com.messenger.inwady.myssenger.model.web.GenerateEvent;

import org.json.JSONException;

public class Authorization extends Fragment {
    App app = null;

    Button logButton;
    Button regButton;
    public EditText pasEd;
    public EditText logEd;
    public Waiter waiter;
    ProgressBar progressBar;

    public LinearLayout linearLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.screen_login,  container, false);
        setHasOptionsMenu(true);

        app = App.getInstance();

        setEvents();
        app.authController = new AuthController(getActivity(), app.lin);

        logButton = (Button) view.findViewById(R.id.button);
        regButton = (Button) view.findViewById(R.id.buttonreg);
        pasEd = (EditText) view.findViewById(R.id.editText2);
        logEd = (EditText) view.findViewById(R.id.editText);
        progressBar = (ProgressBar) view.findViewById(R.id.loginPB);
        progressBar.setVisibility(View.INVISIBLE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            String[] perms = { "android.permission.READ_CONTACTS" };
            int permsCode = 200;

            requestPermissions(perms, permsCode);
        }

        logButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pasEd.getText().toString();
                logEd.getText().toString();
                if ((pasEd.getText().toString().equals("")) || (logEd.getText().equals(""))) {
                    Toast.makeText(getActivity().getBaseContext(), "Please type login and password", Toast.LENGTH_SHORT);
                }
                else {
                    progressBar.setVisibility(View.VISIBLE);

                    app.authController.setPack(logEd.getText().toString(), pasEd.getText().toString());
                    app.authController.startController();

                    waiter = new Waiter(new Waiter.WaiterFailed() {
                        @Override
                        public void doIfFailed() {
                            if (getActivity() != null) { Log.d("No connect!", "Auth"); }
                                /* RECCONECT */
                        }
                    });
                    waiter.startWaiting(20000);
                }

            }
        });
        regButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((Login) getActivity()).toRegistration();

            }
        });

        if(App.mprefs.isValid()) {
            linearLayout = (LinearLayout) view.findViewById(R.id.show);
            linearLayout.setVisibility(View.INVISIBLE);

            auto = true;
            app.authController.setPack(App.mprefs.getLogin(), App.mprefs.getPassword(), false);
            app.authController.startController();
        }

        return view;
    }

    private boolean auto = false;

    private void setEvents() {
        app.lin = new GenerateEvent(new Handler() {
            public void handleMessage(Message msg) {
                String s = Pack.parseMessageJson(msg);
                Pack pack = new Pack(s);
                if(Pack.statusError(pack) == 0) {
                    String sid = pack.data.get("sid");
                    String cid = pack.data.get("cid");

                    app.setMe(new Me(sid, cid, pack.data.get("nick")));

                    if (auto) {
                        app.login = App.mprefs.getLogin();
                    } else {
                        app.saveMe(logEd.getText().toString(), pasEd.getText().toString(), pack.data.get("nick"));
                    }

                    Intent intent = new Intent(App.getInstance(), Main.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    App.getInstance().startActivity(intent);
                    try {
                        getActivity().finish();
                    } catch (Exception e) {

                    }
                } else
                    linearLayout.setVisibility(View.VISIBLE);

            }
        });
    }

}