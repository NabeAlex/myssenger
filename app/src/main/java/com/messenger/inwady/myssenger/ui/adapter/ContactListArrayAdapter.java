package com.messenger.inwady.myssenger.ui.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.messenger.inwady.myssenger.R;
import com.messenger.inwady.myssenger.model.utils.Contact;
import com.messenger.inwady.myssenger.model.utils.ReContact;

import java.util.ArrayList;
import java.util.List;


public class ContactListArrayAdapter extends ArrayAdapter<ReContact> {
    private ArrayList<ReContact> acceptList = new ArrayList<>();
    public ArrayList<ReContact> getAcceptList() {
        return acceptList;
    }
    public void clearList() {
        acceptList.clear();
    }

    private TextView nickText, emailText, phoneText;
    private CheckBox checkBox;

    private List<ReContact> channels = new ArrayList<>();

    @Override
    public void add(ReContact object) {
        channels.add(object);
        super.add(object);
    }

    public ContactListArrayAdapter(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ReContact obj = getItem(position);
        View row = convertView;
        if (row == null) {
            LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.contact_list_item, parent, false);
        }
        nickText = (TextView) row.findViewById(R.id.nick);
        nickText.setText(obj.name);
        emailText = (TextView) row.findViewById(R.id.email);
        emailText.setText(obj.email);
        phoneText = (TextView) row.findViewById(R.id.phone);
        phoneText.setText(obj.phone);

        checkBox = (CheckBox) row.findViewById(R.id.contact_check_box);
        checkBox.setTag(position);
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if (isChecked)
                {
                    acceptList.add(getItem((Integer) buttonView.getTag()));
                } else {
                    acceptList.remove(getItem((Integer) buttonView.getTag()));
                }

            }
        });

        return row;

    }

    public Bitmap decodeToBitmap(byte[] decodedByte) {
        return BitmapFactory.decodeByteArray(decodedByte, 0, decodedByte.length);
    }

}