package com.messenger.inwady.myssenger.model;

import android.os.Handler;
import android.util.Log;

import com.messenger.inwady.myssenger.model.utils.Pack;

import java.util.ArrayList;
import java.util.HashMap;

public class Router {
    private HashMap<String, ArrayList<Handler>> hash = new HashMap<String, ArrayList<Handler>>();

    public void putEvent(String key, Handler handler) {
        Log.d("RequestOn", key);
        if(hash.containsKey(key))
            hash.get(key).add(handler);
        else {
            ArrayList<Handler> ar = new ArrayList<Handler>();
            ar.add(handler);
            hash.put(key, ar);
        }
    }

    public void removeEvent(String key) {
        if(hash.containsKey(key)) {
            ArrayList<Handler> ar = hash.get(key);
            for(Handler i : ar) {
                i.removeCallbacksAndMessages(null);
            }
            ar.clear();
            hash = new HashMap<String, ArrayList<Handler>>();
        }
    }

    public boolean packEvent(String key, String pack) {
        Log.d("Recieve", pack.toString());
        if(!hash.containsKey(key)) return false;
        ArrayList<Handler> ar = hash.get(key);
        for(Handler i : ar) {
            i.handleMessage(Pack.genMessageJson(pack));
        }

        if(key.substring(0, 1).equals("ev"))
            ar.clear();
        return true;
    }

    public static Router getRouter() {
        return App.router;
    }
}
