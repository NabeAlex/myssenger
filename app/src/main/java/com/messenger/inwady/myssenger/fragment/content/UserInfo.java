package com.messenger.inwady.myssenger.fragment.content;

import android.app.Fragment;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.messenger.inwady.myssenger.Main;
import com.messenger.inwady.myssenger.R;
import com.messenger.inwady.myssenger.controller.UserInfoController;
import com.messenger.inwady.myssenger.model.App;
import com.messenger.inwady.myssenger.model.utils.Pack;
import com.messenger.inwady.myssenger.model.web.GenerateEvent;

import butterknife.Bind;
import butterknife.ButterKnife;

public class UserInfo extends Fragment  implements View.OnClickListener {
    public App app = null;
    public Fragment me = null;

    public LayoutInflater inflater = null;
    public void setInflater(LayoutInflater i) {
        inflater = i;
    }


    /*** Controllers ***/

    GenerateEvent user;
    private UserInfoController userInfoController = null;

    /*******************/

    private static View view;
    static {
        view = null;
    }

    @Bind(R.id.info_status)
    public TextView status = null;
    @Bind(R.id.info_nick)
    public TextView nick = null;
    @Bind(R.id.info_email)
    public TextView email = null;
    @Bind(R.id.info_phone)
    public TextView phone = null;

    private String uid = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if(view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }
        try {
            view = inflater.inflate(R.layout.screen_user_info,
                    container, false);
        }catch (InflateException e) {}

        app = App.getInstance();
        App.now = App.Screen.user_info;

        ButterKnife.bind(this, view);

        me = this;

        Bundle bundle = this.getArguments();
        uid = bundle.getString("uid");
        ((Main) getActivity()).setToolbar(uid);

        setEvents();

        status = (TextView)view.findViewById(R.id.info_status);
        nick = (TextView)view.findViewById(R.id.info_nick);

        userInfoController = new UserInfoController(getActivity(), user);
        userInfoController.setPack(app.getMe().sid, app.getMe().cid, uid);
        userInfoController.startController();

        return view;
    }

    private void HandleError(int status) {
        App.Toast(app, "Error!");
    }

    private void setEvents() {
        user = new GenerateEvent(new Handler() {
            public void handleMessage(Message msg) {
                Pack s = Pack.parseMessagePack(msg);
                nick.setText(
                        s.data.get("nick")
                );
                status.setText(
                        s.data.get("user_status")
                );
                email.setText(
                        s.data.get("email")
                );
                phone.setText(s.data.get("phone"));
            }
        });
    }

    @Override
    public void onClick(View v) {

    }
}
