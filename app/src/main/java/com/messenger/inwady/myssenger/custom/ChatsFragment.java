package com.messenger.inwady.myssenger.custom;

import android.app.AlertDialog;
import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.app.Fragment;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.messenger.inwady.myssenger.Main;
import com.messenger.inwady.myssenger.R;

import com.messenger.inwady.myssenger.controller.FriendController;
import com.messenger.inwady.myssenger.controller.MainController;
import com.messenger.inwady.myssenger.custom.dummy.DummyContent;

import com.messenger.inwady.myssenger.data.DBHelper;
import com.messenger.inwady.myssenger.data.Resource;
import com.messenger.inwady.myssenger.fragment.content.ChatField;
import com.messenger.inwady.myssenger.fragment.content.UserInfo;
import com.messenger.inwady.myssenger.model.App;
import com.messenger.inwady.myssenger.model.utils.Pack;
import com.messenger.inwady.myssenger.model.utils.ReContact;
import com.messenger.inwady.myssenger.model.web.GenerateEvent;
import com.messenger.inwady.myssenger.utils.ListUtils;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.sql.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class ChatsFragment extends Fragment implements AbsListView.OnItemClickListener {
    ChatsFragment me = null;
    View view = null;

    public DBHelper db;


    /*** Controllers ***/

    GenerateEvent event;
    private MainController controller = null;

    /*******************/

    private AbsListView myListView;

    /* LEFT MESSAGE */

    private AbsListView leftListView = null;
    /****************/

    public static ChatsFragment newInstance() {
        ChatsFragment fragment = new ChatsFragment();
        Bundle args = new Bundle();
        return fragment;
    }

    public ChatsFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        db = App.getDataBase();

        /* LEAVE LINK */
        Main.setChats(this);
        /*************/

        setLocal();

        controller = new MainController(getActivity());
        controller.setEvent(new GenerateEvent(new Handler() {
            public void handleMessage(Message msg) {
                String s = Pack.parseMessageJson(msg);
                Pack pack = new Pack(s);

                if (Pack.statusError(pack) > 0) {
                } else {
                    String uid;
                    boolean flag = false;

                    ArrayList<String> elementsJSON = new ArrayList<>();

                    try {
                        JSONArray jsonArray = new JSONArray(pack.data.get("list"));
                        JSONObject tmp;
                        for (int i = 0; i < jsonArray.length(); i++) {
                            tmp = jsonArray.getJSONObject(i);
                            uid = tmp.getString("uid");

                            elementsJSON.add(uid);

                            flag = false;
                            for (DummyContent.DummyItem id_ : DummyContent.ITEMS) {
                                Log.d("REWRITE", id_.getChatId());
                                if (uid.equals(id_.getChatId())) {
                                    flag = true;
                                    break;
                                }
                            }
                            if (!flag) {
                                db.insertUser(new ReContact(uid,
                                        tmp.getString("nick"),
                                        tmp.getString("email"),
                                        tmp.getString("phone")), "friend", tmp.getString("picture"));

                            }

                        }

                        for (DummyContent.DummyItem contact : DummyContent.ITEMS) {
                            flag = false;
                            for (String elJSON : elementsJSON)
                                if (elJSON.equals(contact.getChatId())) {
                                    flag = true;
                                    break;
                                }
                            if (!flag)
                                db.deleteUser(contact.getChatId());
                        }

                        updateAll();
                        /* */

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }));
        controller.pushRouter("contactlist");

    }


    private void clearNotify() {
        NotificationManager manager = (NotificationManager) getActivity().getSystemService(Context.NOTIFICATION_SERVICE);
        manager.cancel(0);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        me = this;
        view = inflater.inflate(R.layout.fragment_chats, container, false);

        clearNotify();

        leftListView = (AbsListView) view.findViewById(R.id.list_left_messages);

        showLocalLeftMessages(false);
        showLocalMyMessages(true);

        setListViewHeightBasedOnChildren(leftListView);

        leftListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                DummyContent.LeftDummyItem item = (DummyContent.LeftDummyItem) leftListView.getItemAtPosition(position);
                toChat(item.uid, "NO INFO");
            }
        });
        setListView(leftListView);

        if(App.openUID != null) {
            Bitmap b = db.getUserBitmap(App.openUID);
            toChat(App.openUID, App.openNick, b);

            App.openUID = null;
            App.openNick = null;
        }

        return view;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (null != myListView) {
            DummyContent.DummyItem item = DummyContent.ITEMS.get(position);
            toChat(item.getChatId(), item.name, item.bitmap);
        }
    }

    private void toChat(String uid, String name) {
        App.userBitmapChat = null;
        Fragment fragment = new ChatField();
        Bundle s = new Bundle();
        s.putString("chid", uid);
        s.putString("name", name);
        fragment.setArguments(s);
        Main main = (Main) getActivity();
        main.updateChatFragment(fragment);
    }

    private void toChat(String uid, String name, Bitmap bitmap) {
        App.userBitmapChat = bitmap;
        Fragment fragment = new ChatField();
        Bundle s = new Bundle();
        s.putString("chid", uid);
        s.putString("name", name);
        fragment.setArguments(s);
        Main main = (Main) getActivity();
        main.updateChatFragment(fragment);
    }

    public static void setListViewHeightBasedOnChildren(AbsListView listView) {
        ListUtils.setDynamicHeight((ListView) listView);
        /*ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = 0;
        Log.d("Count", listAdapter.getCount() + "");
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }


        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight
                + (listView.getHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);*/
    }

    public class ChatArrayAdapter extends ArrayAdapter {
        private final Context context;
        private final List<DummyContent.DummyItem> values;

        public ChatArrayAdapter(Context context, List<DummyContent.DummyItem> values) {
            super(context, R.layout.chat_list_item, values);
            this.context = context;
            this.values = values;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.chat_list_item, parent, false);

            TextView textChat = (TextView) view.findViewById(R.id.chat_name);
            TextView textDesc = (TextView) view.findViewById(R.id.chat_description);
            TextView textRead = (TextView) view.findViewById(R.id.no_read);

            ImageView imageView = (ImageView) view.findViewById(R.id.chat_icon);

            textChat.setText(values.get(position).name);
            textDesc.setText(values.get(position).desc);
            if(values.get(position).noRead == 0)
                textRead.setVisibility(View.INVISIBLE);
            else
                textRead.setText(values.get(position).noRead + "");
            if(values.get(position).bitmap != null) {
                Log.d("!!", "IMAGE");
                imageView.setImageBitmap(values.get(position).bitmap);
            }
            return view;
        }
    }

    public class LeftChatArrayAdapter extends ArrayAdapter {
        private final Context context;
        private final List<DummyContent.LeftDummyItem> values;

        public LeftChatArrayAdapter(Context context, List<DummyContent.LeftDummyItem> values) {
            super(context, R.layout.chat_list_item, values);
            this.context = context;
            this.values = values;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.left_contact_list_item, parent, false);

            TextView textChat = (TextView) view.findViewById(R.id.left_uid);
            TextView textRead = (TextView) view.findViewById(R.id.left_no_read);

            textChat.setText(values.get(position).uid);

            if(values.get(position).noRead == 0)
                textRead.setVisibility(View.INVISIBLE);
            else
                textRead.setText(values.get(position).noRead + "");

            return view;
        }
    }


    public void updateAll() {
        setLocal();
        showLocalLeftMessages(false);
        showLocalMyMessages(false);
    }

    private void setLocal() {
        //db.insertUser(new ReContact("dwij", "wije", "oiew", "hedwiowe"), "friend");
        Cursor c = db.getUsers();

        String tmp;

        DummyContent.clearItems();
        DummyContent.clearLeftItems();

        if(c.moveToFirst()) {
            do {
                Log.d("=>", c.getString(c.getColumnIndex(DBHelper.UsersColumns.UID)));
                if (c.getString(c.getColumnIndex(DBHelper.UsersColumns.STATUS)).equals("friend")) {

                    Log.d("Pointer", "" + c.getInt(c.getColumnIndex(DBHelper.UsersColumns.NO_READ)));

                    DummyContent.addItem(new DummyContent.DummyItem(0 + "",
                            c.getString(c.getColumnIndex(DBHelper.UsersColumns.NAME)),
                            c.getString(c.getColumnIndex(DBHelper.UsersColumns.PHONE)),
                            Resource.decodeToBase64(c.getString(c.getColumnIndex(DBHelper.UsersColumns.PICTURE)))).
                            setChatId(c.getString(c.getColumnIndex(DBHelper.UsersColumns.UID))).
                            setNoRead(c.getInt(c.getColumnIndex(DBHelper.UsersColumns.NO_READ))
                            ));

                } else {

                    DummyContent.addLeftItem(new DummyContent.LeftDummyItem(
                            c.getString(c.getColumnIndex(DBHelper.UsersColumns.UID)),
                            c.getInt(c.getColumnIndex(DBHelper.UsersColumns.NO_READ)
                    )));

                }
            } while (c.moveToNext());
        }


        for(String name : db.getAllDialogs()) {
            Log.d("BASE", name);
        }
    }

    public void showLocalLeftMessages(boolean update) {
        if(update) setLocal();

        leftListView = (AbsListView) view.findViewById(R.id.list_left_messages);
        leftListView.setAdapter(new LeftChatArrayAdapter(me.getActivity(), DummyContent.LEFT_ITEMS));
        //leftListView.setOnItemClickListener(me);
        setListViewHeightBasedOnChildren(leftListView);
    }

    public void showLocalMyMessages(boolean update) {
        if(update) setLocal();

        myListView = (AbsListView) view.findViewById(R.id.list_my_messages);
        myListView.setAdapter(new ChatArrayAdapter(me.getActivity(), DummyContent.ITEMS));
        myListView.setOnItemClickListener(me);
        setListView(myListView);
        setListViewHeightBasedOnChildren(myListView);
    }


    private void setListView(AbsListView listView) {
        final String deleteUser = "Delete user";
        final String userInfo = "User Info";

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d("start", "show");

                final CharSequence colors[] = new CharSequence[]{deleteUser, userInfo};

                final String tmpUID = ((DummyContent.LeftDummyItem) parent.getItemAtPosition((position))).uid;
                boolean f = false;
                for(DummyContent.DummyItem a : DummyContent.ITEMS)
                    if(a.uid.equals(tmpUID))
                        f = true;
                final boolean friend = f;

                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("Choose");
                builder.setItems(colors, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (colors[which].toString().equals(userInfo)) {
                            UserInfo userInfo = new UserInfo();
                            Bundle bundle = new Bundle();
                            bundle.putString("uid", tmpUID);
                            userInfo.setArguments(bundle);
                            ((Main) getActivity()).updateChatFragment(userInfo);
                        } else if (colors[which].toString().equals(deleteUser)) {
                            db.deleteUser(tmpUID);
                            updateAll();
                            ArrayList<ReContact> arrayList = new ArrayList<ReContact>();
                            arrayList.add(new ReContact(tmpUID, "", "", ""));
                            if(friend)
                                new FriendController(App.getInstance().getMe(), FriendController.Friend.DELETE, arrayList).start();
                        }
                        dialog.dismiss();
                    }
                });
                builder.show();

                return true;
            }

        });

    }
}