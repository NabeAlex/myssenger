package com.messenger.inwady.myssenger.fragment.content;

import android.Manifest;
import android.app.Fragment;
import android.content.pm.PackageManager;
import android.opengl.Visibility;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.messenger.inwady.myssenger.Main;
import com.messenger.inwady.myssenger.R;
import com.messenger.inwady.myssenger.controller.ContactsController;
import com.messenger.inwady.myssenger.controller.EventsController;
import com.messenger.inwady.myssenger.controller.ImportController;
import com.messenger.inwady.myssenger.model.App;
import com.messenger.inwady.myssenger.model.Waiter;
import com.messenger.inwady.myssenger.model.utils.Contact;
import com.messenger.inwady.myssenger.model.utils.Pack;
import com.messenger.inwady.myssenger.model.utils.ReContact;
import com.messenger.inwady.myssenger.model.web.GenerateEvent;
import com.messenger.inwady.myssenger.ui.CircleProgress;
import com.messenger.inwady.myssenger.ui.adapter.ContactListArrayAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;

import butterknife.Bind;
import butterknife.ButterKnife;

public class Contacts extends Fragment implements View.OnClickListener {

    CircleProgress progress = null;

    @Bind(R.id.contact_list_view)
    public ListView listView;

    @Bind(R.id.email)
    EditText editEmail;
    @Bind(R.id.phone)
    EditText editPhone;
    @Bind(R.id.contacts_search)
    Button buttonSearch;

    @Bind(R.id.contacts_export)
    Button buttonExport;

    @Bind(R.id.waiting_for_contacts)
    RelativeLayout loading;

    @Bind(R.id.info_about_nothing_contacts)
    TextView nothingContacts;

    private ProgressBar progressBar;
    private TextView noContactsTW;

    private ContactListArrayAdapter contactListArrayAdapter;

    /*** Controllers ***/

    GenerateEvent import_;
    private ImportController importController = null;

    /*******************/

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.screen_contacts, container, false);

        ButterKnife.bind(this, view);
        App.now = App.Screen.imports;

        /* PROGRESS */

        progress = new CircleProgress(getActivity(), loading);

        RelativeLayout.LayoutParams rlp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT);
        rlp.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
        progress.setLayoutParams(rlp);

        progress.start();

        /* PROGRESS */

        setField();

        setEvents();

        loading.addView(progress);

        importController = new ImportController(getActivity(), import_);

        contactListArrayAdapter = new ContactListArrayAdapter(getActivity().getBaseContext(), 0 /* WHAT */);
        Main.setContacts(contactListArrayAdapter.getAcceptList());

        listView.setAdapter(contactListArrayAdapter);

        /*
            BUG!

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                Log.d("!!", ((ReContact) parent.getItemAtPosition(position)).name);
            }
        });

        */


        buttonExport.setOnClickListener(this);
        buttonSearch.setOnClickListener(this);

        return view;
    }

    private void setEvents() {
        import_ = new GenerateEvent(new Handler() {
            public void handleMessage(android.os.Message msg) {
                Pack pack = new Pack(Pack.parseMessageJson(msg));

                nowState = State.LIST;
                setField();

                contactListArrayAdapter.clear();
                try {
                    JSONArray jsonArray = new JSONArray(pack.data.get("list"));
                    JSONObject tmp;
                    for(int i = 0; i < jsonArray.length(); i++) {
                        tmp = jsonArray.getJSONObject(i);
                        contactListArrayAdapter.add(new ReContact(tmp.getString("uid"),
                                                                  tmp.getString("nick"),
                                                                  tmp.getString("email"),
                                                                  tmp.getString("phone")));
                    }
                    contactListArrayAdapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });
    }

    @Override
    public void onClick(View v) {
        contactListArrayAdapter.clearList();

        nowState = State.LOADING;
        setField();

        switch (v.getId()) {
            case R.id.contacts_search:

                importController.setPack(new ReContact("", "", editEmail.getText() + "", editPhone.getText() + ""));
                importController.startController();

                break;
            case R.id.contacts_export:

                /* requestPermissions */
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (ContextCompat.checkSelfPermission(getActivity(),
                            Manifest.permission.READ_CONTACTS)
                            != PackageManager.PERMISSION_GRANTED) {

                        ActivityCompat.requestPermissions(getActivity(),
                                new String[]{Manifest.permission.READ_CONTACTS}, 0);
                        return;

                    }
                }
                /* */

                importController.setPack(ImportController.refreshContacts(getActivity()));
                importController.startController();

                break;
        }

    }

    enum State {
        LIST {
            public void setField(View view) {
                if(view.getId() == R.id.contact_list_view) view.setVisibility(View.VISIBLE);
                else view.setVisibility(View.GONE);
            }
        },
        LOADING {
            public void setField(View view) {
                Log.d("BOOL", (view.getId() == R.id.circle_progress) + "");
                if(view.getId() == R.id.circle_progress) view.setVisibility(View.VISIBLE);
                else view.setVisibility(View.GONE);
            }
        },
        NOTHING {
            public void setField(View view) {
                if(view.getId() == R.id.info_about_nothing_contacts) view.setVisibility(View.VISIBLE);
                else view.setVisibility(View.GONE);
            }
        };

        public void setField(View view) {}
    }

    private State nowState = State.LOADING;
    public void setField() {
        nowState.setField(listView);
        nowState.setField(progress);
        nowState.setField(nothingContacts);
    }

}
